<?php

namespace Drupal\charts\Charts;

/**
 * ModuleSelector.
 */
class ModuleSelector {

  private $library;
  private $assetName;
  private $categories;
  private $seriesData;
  private $options;
  private $attachmentDisplayOptions;
  private $chartId;

  /**
   * Construct.
   *
   * @param string $library
   *   Module name.
   * @param array $categories
   *   Categories.
   * @param array $seriesData
   *   Series Data.
   * @param array $options
   *   Options.
   * @param array $attachmentDisplayOptions
   *   AttachmentDisplayOptions.
   * @param string $chartId
   *   Chart Id.
   */
  public function __construct($library = '', array $categories = [], array $seriesData = [], array $options = [], array $attachmentDisplayOptions = [], $chartId = '') {
    $this->library = $library;
    $this->categories = $categories;
    $this->seriesData = $seriesData;
    $this->options = $options;
    $this->attachmentDisplayOptions = $attachmentDisplayOptions;
    $this->chartId = $chartId;
  }

  /**
   * Module exists.
   *
   * @return bool
   *   Module exist.
   */
  public function moduleExists() {
    return \Drupal::moduleHandler()->moduleExists('charts_' . $this->library);
  }

  /**
   * Get class charts render.
   *
   * @return string
   *   Class charts render name.
   */
  private function getClassChartsRender() {
    return 'Drupal\charts_' . $this->library . '\Charts\\' . ucfirst($this->library) . 'ChartsRender';
  }

  /**
   * Build Variables.
   *
   * @param array $variables
   *   Variables.
   */
  public function buildVariables(array &$variables = []) {
    $moduleChartsRenderer = $this->getClassChartsRender();
    if (class_exists($moduleChartsRenderer)) {
      $chartingModule = new $moduleChartsRenderer();
      $chartingModule->chartsRenderCharts($this->options, $this->categories, $this->seriesData, $this->attachmentDisplayOptions, $variables, $this->chartId);
    }
  }

}
