<?php

namespace Drupal\charts\Charts;

/**
 * Defines an interface for charts render classes.
 */
interface ChartsRenderInterface {

  /**
   * Charts render charts.
   *
   * @param array $options
   *   Options.
   * @param array $categories
   *   Categories.
   * @param array $seriesData
   *   Series Data.
   * @param array $attachmentDisplayOptions
   *   Attachment Display Options.
   * @param array $variables
   *   Variables.
   * @param string $chartId
   *   Chart Id.
   */
  public function chartsRenderCharts(array $options = [], array $categories = [], array $seriesData = [], array $attachmentDisplayOptions = [], array &$variables = [], $chartId = '');

}
